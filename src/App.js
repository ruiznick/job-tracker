import React, { Component } from 'react';
import './App.css';
import { JobEntry } from "./Components/JobEntry"
import {AppliedJobs} from "./Components/AppliedJobs";
import {JobEditor} from "./Components/JobEditor";
import {JobExporter} from "./Components/JobExporter";

class App extends Component {
  constructor(props) {
      super(props)

      this.state = {company:"", jobTitle:"", jobLocation:"",
                currentJob: {company:"", jobTitle:"", jobLocation:"",
                dateApplied:"", jobNotes:"", jobStatus:"", jobId:""}}

      this.handleCompanyInput = this.handleCompanyInput.bind(this)
      this.handleJobTitleInput = this.handleJobTitleInput.bind(this)
      this.handleJobLocationInput = this.handleJobLocationInput.bind(this)
      this.addNewJobEntry = this.addNewJobEntry.bind(this)
      this.clearInput = this.clearInput.bind(this)
      this.toggleJobEditor = this.toggleJobEditor.bind(this)
      this.handleJobEditor = this.handleJobEditor.bind(this)
      this.saveUpdatedJob = this.saveUpdatedJob.bind(this)
      this.deleteCurrentJob = this.deleteCurrentJob.bind(this)
      this.importJobData = this.importJobData.bind(this)
  }

  handleCompanyInput(e) {
      this.setState({company: e.target.value})
  }

  handleJobTitleInput(e) {
      this.setState({jobTitle: e.target.value})
  }

  handleJobLocationInput(e) {
      this.setState({jobLocation: e.target.value})
  }

  toggleJobEditor(jobId) {
      let storedJobData = JSON.parse(window.localStorage.getItem("savedJobs"))

      let tCurrentJob = {}

      if (storedJobData.length > 0 && jobId < storedJobData.length) {
          tCurrentJob["company"] = storedJobData[jobId].company
          tCurrentJob["jobTitle"] = storedJobData[jobId].jobTitle
          tCurrentJob["jobLocation"] = storedJobData[jobId].jobLocation
          tCurrentJob["dateApplied"] = storedJobData[jobId].dateApplied
          tCurrentJob["jobNotes"] = storedJobData[jobId].jobNotes
          tCurrentJob["jobStatus"] = storedJobData[jobId].jobStatus
          tCurrentJob["jobId"] = jobId
      }

      this.setState({modal: !this.state.modal,
          currentJob: tCurrentJob
      })
  }

  handleJobEditor(e, jobProperty) {
      let tCurrentJob = this.state.currentJob

      tCurrentJob[jobProperty] = e.target.value

      this.setState({currentJob: tCurrentJob})
  }


  getDate() {
      let date = new Date()
      let mm = date.getMonth() + 1
      let dd = date.getDate()
      let yyyy = date.getFullYear()

      return mm + "/" + dd + "/" + yyyy
  }

  clearInput() {
      this.setState({company: "", jobTitle: "", jobLocation: ""})
  }

  addNewJobEntry() {
      let storedJobData = JSON.parse(window.localStorage.getItem("savedJobs"))
      if (!storedJobData) {
          storedJobData = []
      }
      let newJob = {company: this.state.company,
                    jobTitle: this.state.jobTitle,
                    jobLocation: this.state.jobLocation,
                    dateApplied: this.getDate(),
                    jobStatus: "Awaiting Response"}

      storedJobData.push(newJob)

      const updatedJobs = JSON.stringify(storedJobData)
      window.localStorage.setItem("savedJobs", updatedJobs)

      this.clearInput()
  }

  saveUpdatedJob() {
      let storedJobData = JSON.parse(window.localStorage.getItem("savedJobs"))
      let jobId = this.state.currentJob.jobId

      if (storedJobData.length > 0 && jobId < storedJobData.length) {
          storedJobData[jobId].company = this.state.currentJob.company
          storedJobData[jobId].jobTitle = this.state.currentJob.jobTitle
          storedJobData[jobId].jobLocation = this.state.currentJob.jobLocation
          storedJobData[jobId].dateApplied = this.state.currentJob.dateApplied
          storedJobData[jobId].jobNotes = this.state.currentJob.jobNotes
          storedJobData[jobId].jobStatus = this.state.currentJob.jobStatus
      }

      const updatedJobs = JSON.stringify(storedJobData)
      window.localStorage.setItem("savedJobs", updatedJobs)

      this.setState({modal: false})
  }

  deleteCurrentJob() {
      let storedJobData = JSON.parse(window.localStorage.getItem("savedJobs"))
      let jobId = this.state.currentJob.jobId

      if (storedJobData.length > 0 && jobId < storedJobData.length) {
          storedJobData.splice(jobId, 1)
      }

      const updatedJobs = JSON.stringify(storedJobData)
      window.localStorage.setItem("savedJobs", updatedJobs)

      this.setState({modal: false})
  }

  importJobData(e) {
      var file = e.target.files[0]

      var reader = new FileReader()

      reader.onload = (e) => {
          window.localStorage.setItem("savedJobs", reader.result)
          this.setState({company: ""})
      }

      reader.readAsText(file)
  }

  render() {

    let storedJobData = JSON.parse(window.localStorage.getItem("savedJobs"))
    let jobDataURI = "data:text/plain;charset=utf-8," + encodeURIComponent(window.localStorage.getItem("savedJobs"))

    return (
      <div className="App container">
        <header className="mt-5">
          <h2 className="mb-4">Job Tracker</h2>
        </header>
          <JobEntry company={this.state.company}
                    jobTitle={this.state.jobTitle}
                    jobLocation={this.state.jobLocation}
                    handleCompanyInput={this.handleCompanyInput}
                    handleJobTitleInput={this.handleJobTitleInput}
                    handleJobLocationInput={this.handleJobLocationInput}
                    addNewJobEntry={this.addNewJobEntry}/>

          <br/>
          <AppliedJobs jobs={storedJobData}
                       handleJobEditor={this.toggleJobEditor}
                       companyFilter={this.state.company}
                       jobTitleFilter={this.state.jobTitle}
                       jobLocationFilter={this.state.jobLocation}/>

          <JobEditor modal={this.state.modal} toggleJobEditor={this.toggleJobEditor}
                     currentJob={this.state.currentJob}
                     handleJobEditorInput={this.handleJobEditor}
                     saveJob={this.saveUpdatedJob}
                     deleteJob={this.deleteCurrentJob}/>

          <JobExporter jobData={jobDataURI} handleImport={this.importJobData}/>

          <div className="mt-5 text-secondary">Note: Data is saved locally and not sent to a server. Please use the export/import options to control your data.</div>
      </div>
    );
  }
}

export default App;
