import React from "react"

export class JobEntry extends React.Component {
   render() {
       var addButton
       var inputButton

       if (this.props.company.length > 1 && this.props.jobTitle.length > 1 &&
            this.props.jobLocation.length > 1) {
           addButton = <a href="#" className="btn btn-outline-primary btn-lg" onClick={this.props.addNewJobEntry}>Add</a>

           let handleEnterKey = (e) => {
               if (e.key === "Enter") {
                   this.props.addNewJobEntry()
               }
           }

           inputButton = (<input type="text"
                           id="location"
                           className="form-control"
                           value={this.props.jobLocation}
                           onChange={this.props.handleJobLocationInput}
                           onKeyPress={handleEnterKey}/>)
       } else {
           addButton = <a href="#" className="btn btn-outline-dark btn-lg disabled">Add</a>
           inputButton = (<input type="text"
                           id="location"
                           className="form-control"
                           value={this.props.jobLocation}
                           onChange={this.props.handleJobLocationInput}/>)
       }
       return (<div>
           <div className="row">
               <div className="input-group col-md-4">
                   <span className="input-group-addon">Company</span>
                   <input type="text"
                          id="company"
                          className="form-control"
                          value={this.props.company}
                          onChange={this.props.handleCompanyInput}/>
               </div>

               <br/>

               <div className="input-group col-md-4">
                    <span className="input-group-addon">Job Title</span>
                    <input type="text"
                           id="jobTitle"
                           className="form-control"
                           value={this.props.jobTitle}
                           onChange={this.props.handleJobTitleInput}/>
               </div>

               <br/>

               <div className="input-group col-md-4">
                    <span className="input-group-addon">Location</span>
                    {inputButton}
               </div>
           </div>

           <div className="mt-2">
               {addButton}
           </div>
       </div>)
   }
}