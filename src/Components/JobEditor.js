import React from "react"
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from "reactstrap"

export class JobEditor extends React.Component {
    render() {
        return (<div>
            <Modal isOpen={this.props.modal} toggle={this.props.toggleJobEditor}>
                <ModalHeader toggle={this.props.toggleJobEditor}>Job Editor</ModalHeader>
                <ModalBody>
                    <div className="form-group">
                        <label>Company</label>
                        <input type="text" className="form-control"
                               value={this.props.currentJob.company}
                               onChange={(e) => this.props.handleJobEditorInput(e, "company")}/>
                    </div>

                    <div className="form-group">
                        <label>Job Title</label>
                        <input type="text" className="form-control"
                               value={this.props.currentJob.jobTitle}
                               onChange={(e) => this.props.handleJobEditorInput(e, "jobTitle")}/>
                    </div>

                    <div className="form-group">
                        <label>Location</label>
                        <input type="text" className="form-control"
                               value={this.props.currentJob.jobLocation}
                               onChange={(e) => this.props.handleJobEditorInput(e, "jobLocation")}/>
                    </div>

                    <div className="form-group">
                        <label>Date Applied</label>
                        <input type="text" className="form-control"
                               value={this.props.currentJob.dateApplied}
                               onChange={(e) => this.props.handleJobEditorInput(e, "dateApplied")}/>
                    </div>

                    <div className="form-group">
                        <label>Notes</label>
                        <textarea type="text" className="form-control"
                                  value={this.props.currentJob.jobNotes}
                                  onChange={(e) => this.props.handleJobEditorInput(e, "jobNotes")}/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="inputStatus">Status</label>
                        <select id="inputStatus"
                                className="form-control"
                                value={this.props.currentJob.jobStatus}
                                onChange={(e) => this.props.handleJobEditorInput(e, "jobStatus")}>
                            <option value="Awaiting Response">Awaiting Response</option>
                            <option value="Phone/Video Screen Scheduled">Phone/Video Screen Scheduled</option>
                            <option value="Started Interview Process">Started Interview Process</option>
                            <option value="Rejected/Declined Offer">Rejected/Declined Offer</option>
                        </select>
                    </div>

                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.props.saveJob}>Save</Button>
                    <Button color="danger" onClick={this.props.deleteJob}><i className="fa fa-trash fa-lg"> </i> Delete</Button>
                </ModalFooter>
            </Modal>
        </div>)
    }
}