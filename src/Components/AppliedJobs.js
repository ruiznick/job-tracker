import React from "react"

export class AppliedJobs extends React.Component {

    constructor(props) {
        super(props)

        this.filterJob = this.filterJob.bind(this)
    }

    filterJob(company, jobTitle, jobLocation) {
        var hasCompany = this.props.companyFilter.length > 1
        var hasJobTitle = this.props.jobTitleFilter.length > 1
        var hasJobLocation = this.props.jobLocationFilter.length > 1

        if (hasCompany) {
            let companyRE = new RegExp(this.props.companyFilter, "i")

            if (!companyRE.test(company)) {
               return true
            }
        }

        if (hasJobTitle) {
            let jobTitleRE = new RegExp(this.props.jobTitleFilter, "i")

            if (!jobTitleRE.test(jobTitle)) {
               return true
            }
        }

        if (hasJobLocation) {
            let jobLocationRE = new RegExp(this.props.jobLocationFilter, "i")

            if (!jobLocationRE.test(jobLocation)) {
               return true
            }
        }

        return false
    }

    render() {
        let jobData = []
        let tableStyle = {}
        let visibleRows = 0

        if (this.props.jobs) {
            for (let i = this.props.jobs.length-1; i >= 0; i--) {

                if (this.filterJob(this.props.jobs[i].company,
                        this.props.jobs[i].jobTitle,
                        this.props.jobs[i].jobLocation)) {
                    continue
                }

                visibleRows++

                let rowColor = ""

                switch(this.props.jobs[i].jobStatus) {
                    case "Phone/Video Screen Scheduled":
                        rowColor="bg-warning"
                        break

                    case "Started Interview Process":
                        rowColor="bg-success"
                        break

                    case "Rejected/Declined Offer":
                        rowColor="bg-danger"
                        break
                }

                let tJob = <tr onClick={() => this.props.handleJobEditor(i)}
                               className={rowColor}>
                    <th>{i+1}</th>
                    <td>{this.props.jobs[i].company}</td>
                    <td>{this.props.jobs[i].jobTitle}</td>
                    <td>{this.props.jobs[i].jobLocation}</td>
                    <td>{this.props.jobs[i].dateApplied}</td>
                </tr>

                jobData.push(tJob)
            }
        }

        if (visibleRows > 14) {
            tableStyle = {overflow:"auto", height:"700px"}
        }

        return (<div style={tableStyle}>
            <table className="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Company</th>
                    <th>Job Title</th>
                    <th>Location</th>
                    <th>Date Applied</th>
                </tr>
                </thead>
                <tbody>
                    {jobData}
                </tbody>
            </table>
        </div>)
    }
}