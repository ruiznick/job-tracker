import React from "react"

export class JobExporter extends React.Component {
    render() {
        return (<div className="mt-5">
            <a href={this.props.jobData}
               className="btn btn-secondary"
               download="jobTracker.json">Export</a>

                <label className="btn btn-primary ml-1 mt-2">Import<input type="file"
                       onChange={this.props.handleImport} hidden/></label>
        </div>)
    }
}